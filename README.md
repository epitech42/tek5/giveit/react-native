# react-native

Requirements:
   - docker
   - docker-compose
   - make

Don't forget to init, if not, will not works
After init, correct your local IP Adress if not good.

Init Project:
	```make init```

Launch attached in term:
	```make dev```

Launch:
	```make``` or ```make start```
	
Stop:
	```make stop```

Restart:
	```make re```
	
Delete containers:
	```make clean```

Logs:
    ```make logs```
    
Tree:
    ```make tree```
    
Linter:
    ```make linter```