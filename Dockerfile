FROM node:12-stretch
WORKDIR /usr/src/app

# Requirements of bcrypt
#RUN apk update
#RUN apk --no-cache add --virtual builds-deps build-base python git

RUN apt-get update
RUN apt-get install -yy git

#RUN npm set config registry http://172.42.42.42:4873

RUN npm install -g expo-cli

# Copy package.json
COPY ./src/package* ./

# Install Expo install
RUN expo install

# Install dep
RUN npm install -i

CMD ["npm", "run", "start"]
