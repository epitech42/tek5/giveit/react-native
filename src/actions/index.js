import socketActions from './socket.actions';

const allActions = {
  socketActions,
};

export default allActions;
