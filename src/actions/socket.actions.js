import {
  ADD_PRODUCT, DELETE_PRODUCT, GET_PRODUCT_INFO, GET_SERVER_ANSWER, USER_AUTH,
} from './types';

const addProduct = (title, content) => (
  {
    type: ADD_PRODUCT,
    data: {
      title,
      content,
    },
  }
);

const deleteProduct = (productID) => (
  {
    type: DELETE_PRODUCT,
    data: {
      productID,
    },
  }
);

const getProductInfo = (productID) => (
  {
    type: GET_PRODUCT_INFO,
    data: {
      productID,
    },
  }
);

const getServerAnswer = (url) => (
  {
    type: GET_SERVER_ANSWER,
    data: {
      url,
    },
  }
);

const auth = (token) => (
  {
    type: USER_AUTH,
    data: {
      token,
    },
  }
);


const socketActions = {
  addProduct,
  deleteProduct,
  getProductInfo,
  getServerAnswer,
  auth,
};

export default socketActions;
