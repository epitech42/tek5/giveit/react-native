const userRegisterEvent = 'user:register';
const userLoginEvent = 'user:login';
const userAuthEvent = 'user:auth';

const userLockEvent = 'user:lock';
const userUnlockEvent = 'user:unlock';

const userDestroyEvent = 'user:destroy';
const userGetInfosEvent = 'user:getinfos';

const productAddEvent = 'product:add';
const productListEvent = 'product:list';
const productEditEvent = 'product:edit';
const productDeleteEvent = 'product:delete';
const productUploadPictureEvent = 'product:uploadPicture';
const productGetInfos = 'product:getInfos';
const productGetMyProducts = 'product:getMyProducts';

const userGetUserFromUsernameEvent = 'user:getUserFromUsername';
const userGetUserFromIDEvent = 'user:getUserFromID';
const userGetUsersFromIDsEvent = 'user:getUsersFromIDs';

const userEditUsernameEvent = 'user:edit:username';
const userEditPasswordEvent = 'user:edit:password';
const userUploadPictureEvent = 'user:uploadPicture';

const success = ':success';
const err = ':err';

const apiUrl = 'https://api.dev.giveit.grosseteub.fr';
// const apiUrl = "http://192.168.0.13:1337";

export default {
  userRegisterEvent,
  userLoginEvent,
  userAuthEvent,
  userLockEvent,
  userUnlockEvent,
  userDestroyEvent,
  userGetInfosEvent,
  productAddEvent,
  productListEvent,
  productEditEvent,
  productDeleteEvent,
  productUploadPictureEvent,
  productGetInfos,
  productGetMyProducts,
  userGetUserFromUsernameEvent,
  userGetUserFromIDEvent,
  userGetUsersFromIDsEvent,
  userEditUsernameEvent,
  userEditPasswordEvent,
  userUploadPictureEvent,
  success,
  err,
  apiUrl, // tomove
};
