import * as React from 'react';
import PropTypes from 'prop-types';
import {
  Platform, StatusBar, StyleSheet, View,
} from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { Provider } from 'react-redux';
import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import ViewProductPage from './screens/ViewProductPage';
import MyProductsPage from './screens/MyProductsPage';
import ProfileEditPage from './screens/ProfileEditPage';
import ProfilePage from './screens/ProfilePage';
import initScreen from './screens/initScreen';
import useLinking from './navigation/useLinking';

import ProductFormPage from './screens/ProductFormPage';

import Signup from './screens/Signup';
import Login from './screens/Login';
import configureStore from './configureStore';

const Stack = createStackNavigator();

const store = configureStore();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const { skipLoadingScreen } = props;
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !skipLoadingScreen) {
    return null;
  }
  return (
    <Provider store={store}>
      <ActionSheetProvider>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
            <Stack.Navigator
              initialRouteName="initScreen"
              headerMode="screen"
              screenOptions={{
                headerTintColor: 'white',
                headerStyle: { backgroundColor: '#00CCFF' },
              }}
            >
              <Stack.Screen name="initScreen" component={initScreen} />
              <Stack.Screen name="Signup" component={Signup} />
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="ProductFormPage" component={ProductFormPage} />
              <Stack.Screen name="ViewProductPage" component={ViewProductPage} />
              <Stack.Screen name="MyProductsPage" component={MyProductsPage} />
              <Stack.Screen name="ProfileEditPage" component={ProfileEditPage} />
              <Stack.Screen name="ProfilePage" component={ProfilePage} />
              <Stack.Screen name="BottomTabNavigator" component={BottomTabNavigator} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </ActionSheetProvider>
    </Provider>
  );
}


App.propTypes = {
  skipLoadingScreen: PropTypes.bool,
};

App.defaultProps = {
  skipLoadingScreen: false,
};
