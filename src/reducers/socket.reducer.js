import Socket from '../services/socket';

const initialState = {
  socket: new Socket(),
};

const socketReducer = (state = initialState) => ({ ...state });
export default socketReducer;
