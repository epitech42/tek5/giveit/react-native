/* eslint no-case-declarations: "off" */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, Alert,
  ActivityIndicator,
} from 'react-native';

import { useNavigation } from '@react-navigation/native';
import { Image } from 'react-native-elements';

import { useSelector } from 'react-redux';
import userService from '../services/user';

const Logo = require('../assets/images/giveit.png');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputBox: {
    width: 300,
    backgroundColor: '#eeeeee',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#002f6c',
    marginVertical: 10,
  },
  button: {
    width: 300,
    backgroundColor: '#4f83cc',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 12,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
});


export default function Form({ type }) {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const navigation = useNavigation();
  const socket = useSelector((store) => store.socketReducer.socket);

  const saveData = async () => {
    if (type !== 'Login') {
	    if (!email || !password) {
        Alert.alert('Invalid Form', 'Username or Password is empty');
      } else {
        await socket.register(email, password);
        const data = await socket.getServerAnswer('user:register');
        console.log(`DATA : [ ${data.message} ] || [ ${data} ] `);
        if (data && data === 'USER_FOUND') {
          Alert.alert('Username already taken', 'Please choose another username');
        } else if (data && data.message === 'USER_CREATED') {
          Alert.alert('User created', 'You are going to be redirected to the login page');
          navigation.navigate('Login');
        }
	    }
      Keyboard.dismiss();
    } else if (type === 'Login') {
	    if (!email || !password) {
        Alert.alert('Invalid Form', 'Username or Password is empty');
      } else {
        await socket.login(email, password);
        const data = await socket.getServerAnswer('user:login');
        // console.log(data["token"]);
        switch (data) {
          case 'WRONG_PASSWORD':
            Alert.alert('Invalid information', 'Invalid password or username');
            break;
          case 'USER_NOT_FOUND':
            Alert.alert('User not found', 'You need to register first');
            break;
          default:
            await socket.auth(data.token);
            const ret = await socket.getServerAnswer('user:auth');
            if (ret === 'CONNECTED') {
              await userService.SaveToken(data.token);
              await userService.SaveUsername(data.username);
              await userService.SaveUserID(data.id);
              navigation.reset({
		    index: 0,
		    routes: [{ name: 'BottomTabNavigator' }],
              });
            }
        }
      }
    }
  };

  return (
    <View style={styles.container}>
      <Image
        source={Logo}
        style={{ width: 200, height: 200 }}
        PlaceholderContent={<ActivityIndicator />}
      />

      <TextInput
        style={styles.inputBox}
        onChangeText={(input) => setEmail(input)}
        underlineColorAndroid='rgba(0,0,0,0)'
        placeholder="Username"
        placeholderTextColor="#002f6c"
        selectionColor="#fff"
        keyboardType="email-address"
        onSubmitEditing={() => password.focus()}
      />

      <TextInput
        style={styles.inputBox}
        onChangeText={(input) => setPassword(input)}
        underlineColorAndroid='rgba(0,0,0,0)'
        placeholder="Password"
        secureTextEntry
        placeholderTextColor="#002f6c"
      // ref={(input) => setPassword(input)}
      />

      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText} onPress={saveData}>{type}</Text>
      </TouchableOpacity>
    </View>

  );
}

Form.propTypes = {
  type: PropTypes.string.isRequired,
};
