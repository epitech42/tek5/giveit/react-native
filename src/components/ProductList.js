import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { RefreshControl } from 'react-native';
import { useSelector } from 'react-redux';
import userService from '../services/user';
import Events from '../constants/events';
import ProductListItem from './ProductListItem';

const ProductList = ({ type }) => {
  const [products, setProducts] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const socket = useSelector((store) => store.socketReducer.socket);
  const navigation = useNavigation();


  const fetch = () => {
    if (type === 'My') {
      socket.getMyProducts().then(() => {
        socket.getServerAnswer(Events.productGetMyProducts).then((result) => {
	    let data = result;
	    data = data.sort((a, b) => new Date(b.updated) - new Date(a.updated));
	    userService.LoadUsername().then((username) => {
		  for (let i = 0; i < data.length; i += 1) {
		      data[i].updated = new Date(data[i].updated).toLocaleString();
		      data[i].createdDate = new Date(data[i].created).toLocaleDateString();
		      data[i].created = new Date(data[i].created).toLocaleString();
		      data[i].createdBy = username;
		  }
		  setProducts(data);
		  setRefreshing(false);
	      });
        });
      });
    } else if (type === 'All') {
      socket.getAllProducts().then(() => {
        socket.getServerAnswer(Events.productListEvent).then((result) => {
	    let data = result;
          const tofound = [];
	    data = data.sort((a, b) => new Date(b.updated) - new Date(a.updated));
          for (let i = 0; i < data.length; i += 1) {
            let found = false;
            data[i].updated = new Date(data[i].updated).toLocaleString();
	      data[i].createdDate = new Date(data[i].created).toLocaleDateString();
            data[i].created = new Date(data[i].created).toLocaleString();
            // IF ALREADY IN DICO, DON'T NEED TO FETCH AGAIN
            for (let n = 0; n < tofound.length; n += 1) {
              if (tofound[n] === data[i].createdBy) {
                found = true;
              }
            }
            if (found === false) tofound.push(data[i].createdBy);
          }
          socket.getUsersFromIDs(tofound).then(() => {
            socket.getServerAnswer(Events.userGetUsersFromIDsEvent).then((users) => {
              for (let p = 0; p < data.length; p += 1) {
                for (let n = 0; n < users.length; n += 1) {
                  if (data[p].createdBy === users[n]._id) data[p].createdBy = users[n].username;
                }
              }
              setProducts(data);
              setRefreshing(false);
            });
          });
        });
      });
    }
  };

  useEffect(() => {
    fetch();
  }, [refreshing]);
  return (
    <ScrollView
      refreshControl={(
        <RefreshControl
          refreshing={refreshing}
          onRefresh={fetch}
        />
              )}
    >
      {
          products.map((product) => (
            <ProductListItem key={product._id} product={product} navigation={navigation} />
          ))
            }
    </ScrollView>
  );
};

ProductList.propTypes = {
  type: PropTypes.string.isRequired,
};

export default ProductList;
