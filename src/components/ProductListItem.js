import PropTypes from 'prop-types';
import { StyleSheet, TouchableHighlight, View } from 'react-native';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { Card, Image, Text } from 'react-native-elements';
import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';

const styles = StyleSheet.create({
  productListItemView: {
  },
  productListItemGrid: {
  },
  productListItemCol: {
    padding: 15,
  },
  productListItemImageRow: {
    display: 'flex',
    paddingBottom: 5,
  },
  productListItemImageContainer: {
    borderRadius: 5,
    flex: 1,
    height: 200,
    margin: 0,
    padding: 0,
  },
  productListItemImage: {
    borderRadius: 5,
    height: '100%',
  },
  productListItemTitle: {
    color: '#3F4E50',
    fontSize: 20,
    fontWeight: 'bold',
  },
  productListItemSubtitle: {
    fontWeight: 'bold',
  },
  productListItemContent: {
    color: '#3F4E50',
    fontSize: 15,
    fontWeight: '300',
  },
  productListItemCardChip: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: '#3F4E50',
    position: 'absolute',
    top: 10,
    right: 10,
  },
  productListItemCardChipContent: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
});

const ProductListItem = ({ product }) => {
  const navigation = useNavigation();
  let pictureUrl = null;
  const [decodedPicture, setDecodedPicture] = useState('');
  useEffect(() => {
    if (product.picture) {
      pictureUrl = `https://cloudflare-ipfs.com/ipfs/${product.picture}`;
      fetch(pictureUrl).then((response) => {
        response.text().then((data) => {
          console.log('Picture size: ', data.length);
          setDecodedPicture(`data:image/png;base64,${data}`);
        });
      });
    }
  }, [product, decodedPicture]);
  const onPress = () => {
    navigation.navigate('ViewProductPage', { id: product._id, title: product.title });
  };
  return (
    <TouchableHighlight underlayColor="#DDDDDD" onPress={onPress}>

      <View style={styles.productListItemView}>
        <Grid style={styles.productListItemGrid}>
          <Col style={styles.productListItemCol}>
            <Row style={styles.productListItemImageRow}>
              <Card containerStyle={styles.productListItemImageContainer}>
                <Image
                  style={styles.productListItemImage}
                  resizeMode='contain'
                  source={{
                    uri: decodedPicture || 'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png',
                  }}
                />
                <View style={styles.productListItemCardChip}>
                  <Text style={styles.productListItemCardChipContent}>{product.createdDate}</Text>
                </View>
              </Card>
            </Row>
            <Row>
              <Text style={styles.productListItemTitle}>{product.title}</Text>
            </Row>
            <Row>
              <Text style={styles.productListItemContent}>
                <Text style={styles.productListItemSubtitle}>Last update:&nbsp;</Text>
                {product.updated}
              </Text>
            </Row>
            <Row>
              <Text style={styles.productListItemContent}>
                <Text style={styles.productListItemSubtitle}>Created by:&nbsp;</Text>
                {product.createdBy}
              </Text>
            </Row>
          </Col>
        </Grid>
      </View>
    </TouchableHighlight>

  );
};

ProductListItem.propTypes = {
  product: PropTypes.shape({
	    created: PropTypes.string,
	    updated: PropTypes.string,
    createdBy: PropTypes.string,
    createdDate: PropTypes.string,
	    title: PropTypes.string,
    _id: PropTypes.string,
    picture: PropTypes.string,
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,

};

export default ProductListItem;
