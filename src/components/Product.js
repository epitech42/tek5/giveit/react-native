import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Grid, Row, Col } from 'react-native-easy-grid';
import {
  Button, Image, Text, Divider,
} from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import Events from '../constants/events';

const styles = StyleSheet.create({
  mainView: {
    height: '100%',
  },
  grid: {
  },
  col: {
  },
  imageRow: {
    display: 'flex',
    height: 250,
    paddingBottom: 10,
  },
  addImageButton: {
    backgroundColor: '#3F4E50',
  },
  addImageButtonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
    borderRadius: 20,
  },
  addImageButtonTitle: {
    fontSize: 14,
  },
  actionButtonsRow: {
  },
  buyButtonContainer: {
    flex: 1,
    padding: 5,
  },
  buyButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'green',
    borderRadius: 10,
  },
  buyButtonIcon: {
    color: 'green',
  },
  buyButtonTitle: {
    color: 'green',
  },
  editButtonContainer: {
    flex: 1,
    padding: 5,
  },
  editButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'brown',
    borderRadius: 10,
  },
  editButtonIcon: {
    color: 'brown',
  },
  editButtonTitle: {
    color: 'brown',
  },
  deleteButtonContainer: {
    flex: 1,
    padding: 5,
  },
  deleteButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'red',
    borderRadius: 10,
  },
  deleteButtonIcon: {
    color: 'red',
  },
  deleteButtonTitle: {
    color: 'red',
  },
  imageWrapper: {
    flex: 1,
    height: '100%',
  },
  image: {
    height: '100%',
  },
  inputRow: {
    height: 100,
  },
  titleRow: {
    padding: 15,
  },
  contentRow: {
    padding: 15,
  },
  descriptionRow: {
  },
  title: {
    color: '#3F4E50',
    fontSize: 20,
    fontWeight: 'bold',
  },
  content: {
    color: '#3F4E50',
    fontSize: 15,
  },
  subtitle: {
    color: '#3F4E50',
    fontSize: 15,
    fontWeight: 'bold',
  },
  dateCreatedContainer: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: '#3F4E50',
    position: 'absolute',
    top: 10,
    right: 10,
  },
  dateCreatedContent: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
});


const Product = ({ product, isMine }) => {
  const navigation = useNavigation();
  const socket = useSelector((state) => state.socketReducer.socket);
  let pictureUrl = null;
  const [decodedPicture, setDecodedPicture] = useState('');
  useEffect(() => {
    if (product.picture) {
      pictureUrl = `https://cloudflare-ipfs.com/ipfs/${product.picture}`;
      fetch(pictureUrl).then((response) => {
        response.text().then((data) => {
          console.log('Picture size: ', data.length);
          setDecodedPicture(`data:image/png;base64,${data}`);
        });
      });
    }
  }, [product, decodedPicture]);
  const onPressDelete = () => {
    console.log(`delete ${product._id}`);
    socket.deleteProduct(product._id);
    socket.getServerAnswer(Events.productDeleteEvent).then((data) => {
	  console.log(data);
	  navigation.goBack();
    });
  };

  const onPressEdit = () => {
    console.log(product);
    navigation.navigate('ProductFormPage', { product });
  };

  return (
    <View style={styles.mainView}>
      <Grid style={styles.grid}>
        <Col style={styles.col}>
          <Row style={styles.imageRow} size={12}>
            <View style={styles.imageWrapper}>
              <Image
                style={styles.image}
                resizeMode="contain"
                source={{
                  uri: decodedPicture || 'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png',
                }}
              />
              <View style={styles.dateCreatedContainer}>
                <Text style={styles.dateCreatedContent}>{product.created}</Text>
              </View>
            </View>
          </Row>
          <Divider style={{ backgroundColor: 'grey', marginBottom: 10 }} />
          <Row size={1} style={{ paddingLeft: 15 }}>
            <Text style={styles.subtitle}>Last update: </Text>
            <Text style={styles.content}>{product.updated}</Text>
          </Row>
          <Row size={1} style={{ paddingLeft: 15 }}>
            <Text style={styles.subtitle}>Created by: </Text>
            <Text style={styles.content}>{product.createdBy}</Text>
          </Row>
          <Divider style={{ backgroundColor: 'grey', marginTop: 10 }} />
          <Row style={styles.titleRow} size={1}>
            <Text style={styles.title}>NAME</Text>
          </Row>
          <Row style={styles.contentRow} size={1}>
            <Text style={styles.content}>{product.title}</Text>
          </Row>
          <Row style={styles.titleRow} size={1}>
            <Text style={styles.title}>DESCRIPTION</Text>
          </Row>
          <Row size={8}>
            <Col>
              <Row style={styles.contentRow} size={4}>
                <ScrollView>
                  <Text style={styles.content}>{product.content}</Text>
                </ScrollView>
              </Row>
            </Col>
          </Row>
          {!isMine
            ? (
              <Row size={3}>
                <Button
                  titleStyle={styles.buyButtonTitle}
                  containerStyle={styles.buyButtonContainer}
                  buttonStyle={styles.buyButton}
                  icon={{
                    name: 'money',
                    type: 'font-awesome',
                    size: 14,
                    color: 'green',
                  }}
                  title="Buy product"
                />
              </Row>
            )
            : (
              <Row size={3}>
                <Col size={6}>
                  <Button
                    onPress={onPressEdit}
                    titleStyle={styles.editButtonTitle}
                    containerStyle={styles.editButtonContainer}
                    buttonStyle={styles.editButton}
                    icon={{
                      name: 'pencil',
                      type: 'font-awesome',
                      size: 14,
                      color: 'brown',
                    }}
                    title="Edit product"
                  />
                </Col>
                <Col size={6}>
                  <Button
                    onPress={onPressDelete}
                    titleStyle={styles.deleteButtonTitle}
                    containerStyle={styles.deleteButtonContainer}
                    buttonStyle={styles.deleteButton}
                    icon={{
                      name: 'times',
                      type: 'font-awesome',
                      size: 14,
                      color: 'red',
                    }}
                    title="Delete product"
                  />
                </Col>
              </Row>
            )}
        </Col>
      </Grid>
    </View>
  );
};

Product.propTypes = {
  isMine: PropTypes.bool.isRequired,
  product: PropTypes.shape({
	    created: PropTypes.string,
	    updated: PropTypes.string,
	    createdBy: PropTypes.string,
	    title: PropTypes.string,
	    content: PropTypes.string,
    _id: PropTypes.string,
    picture: PropTypes.string,
  }).isRequired,
};

export default Product;
