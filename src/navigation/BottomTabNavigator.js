/* eslint react/no-unused-prop-types: "off" */
import * as React from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBarIcon from '../components/TabBarIcon';
import AllProductsPage from '../screens/AllProductsPage';
import AddProductPage from '../screens/ProductFormPage';
import ProfilePage from '../screens/ProfilePage';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'AllProducts';

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'AllProducts':
      return 'All Products';
    case 'AddProduct':
      return 'Add Product';
    case 'Profile':
      return 'Profile Page';
    default:
      return INITIAL_ROUTE_NAME;
  }
}

export default function BottomTabNavigator({ route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  const navigation = useNavigation();
  navigation.setOptions({ headerTitle: getHeaderTitle(route) });
  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="AllProducts"
        component={AllProductsPage}
        options={{
          title: 'All Products',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-list" />,
        }}
      />
      <BottomTab.Screen
        name="AddProduct"
        component={AddProductPage}
        options={{
          title: 'Add Product',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-add-circle" />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfilePage}
        options={{
	  title: 'Profile Page',
	  tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-person" />,
        }}
      />
    </BottomTab.Navigator>
  );
}


BottomTabNavigator.propTypes = {
  navigation: PropTypes.shape({
    setOptions: PropTypes.func,
  }).isRequired,
  route: PropTypes.shape({
  }).isRequired,
  focused: PropTypes.bool,
};

BottomTabNavigator.defaultProps = {
  focused: false,
};
