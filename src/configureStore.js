import { createStore, combineReducers } from 'redux';
import socketReducer from './reducers/socket.reducer';

const rootReducer = combineReducers({
  socketReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;

/* export default function configureStore(initialState, socketClient, apiClient) {
    const loggerMiddleware = createLogger();
    const middleware = [
        socketMiddleware(socketClient),
    ];
} */
