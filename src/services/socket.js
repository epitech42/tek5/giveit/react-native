import socketIO from 'socket.io-client';

import userService from './user';

import Events from '../constants/events';

export default class Socket {
  constructor() {
    this.data = null;
    this.socket = socketIO(Events.apiUrl, {
      transports: ['websocket'],
      jsonp: false,
    });
    this.socket.connect();
    this.socket.on('connect', () => {
      // alert('connected to socket server');
      console.log('connected to socket server');
    });
    this.socket.on('disconnect', () => {
      // alert('disconnected')
      console.log('disconnected');
    });
  }

  async register(username: string, password: string) {
    await this.socket.emit(Events.userRegisterEvent, username, password);
  }

  async login(username: string, password: string) {
    await this.socket.emit(Events.userLoginEvent, username, password);
  }

  async auth(token: string) {
    const Token = token || await userService.LoadToken();
    await this.socket.emit(Events.userAuthEvent, Token);
  }

  async getAllProducts() {
    await this.socket.emit(Events.productListEvent, '');
  }

  async addProduct(title: string, content: string) {
    await this.socket.emit(Events.productAddEvent, title, content);
  }

  async editProduct(productID: string, title: string, content: string) {
    await this.socket.emit(Events.productEditEvent, productID, title, content);
  }

  async deleteProduct(productID: string) {
    await this.socket.emit(Events.productDeleteEvent, productID);
  }

  async uploadProductPicture(productID: string, picture: string) {
    await this.socket.emit(Events.productUploadPictureEvent, productID, picture);
  }

  async uploadUserPicture(picture: string) {
    await this.socket.emit(Events.userUploadPictureEvent, picture);
  }

  async editUsername(username: string) {
    await this.socket.emit(Events.userEditUsernameEvent, username);
  }

  async editPassword(oldPassword: string, newPassword: string) {
    await this.socket.emit(Events.userEditPasswordEvent, oldPassword, newPassword);
  }

  async getUserFromUsername(username: string) {
    await this.socket.emit(Events.userGetUserFromUsernameEvent, username);
  }

  async getUserFromID(userID: string) {
    await this.socket.emit(Events.userGetUserFromIDEvent, userID);
  }

  async getUsersFromIDs(userIDs: Array) {
    await this.socket.emit(Events.userGetUsersFromIDsEvent, userIDs);
  }

  async getMyProfile() {
    const username = await userService.LoadUsername();
    await this.socket.emit(Events.userGetUserFromUsernameEvent, username);
  }

  async getProductInfos(productID: string) {
    await this.socket.emit(Events.productGetInfos, productID);
  }

  async getMyProducts() {
    await this.socket.emit(Events.productGetMyProducts);
  }

  async getServerAnswer(url: string) {
    const { socket } = this;
    return new Promise(((resolve) => {
      socket.on(url + Events.success, (data) => {
        console.log(`${url} :success : ${data}`);
		 // socket.off(url + Events.success);
        resolve(data);
      });
      socket.on(url + Events.err, (err) => {
        console.log(`${url} err: ${err}`);
		 // socket.off(url + Events.err);
		 if (err === 'USER_NOT_CONNECTED') {
          socket.auth().then(() => {
			 console.log('back in game');
		     });
        }
        resolve(err);
      });
    }));
  }
}
