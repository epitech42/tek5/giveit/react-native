import * as ImagePicker from 'expo-image-picker';
import * as ImageManipulator from 'expo-image-manipulator';

export default class ImageService {
    pickImage = async () => new Promise((resolve, reject) => {
      ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      }).then((result) => {
        if (result.cancelled === true) {
	      reject(new Error('Image picking cancelled'));
        } else resolve(result.uri);
      });
    }).catch((error) => {
      console.log(`error ${error}`);
      throw new Error(error);
    });

    openCamera = () => new Promise((resolve) => {
      ImagePicker.launchCameraAsync().then((cameraResponse) => {
        resolve(cameraResponse.uri);
        console.log(cameraResponse);
      });
    });

    compressImage = (uri) => new Promise((resolve) => {
      ImageManipulator.manipulateAsync(uri, [], { compress: 0.2 }).then((compressedImage) => {
	    resolve(compressedImage.uri);
      });
    });
}
