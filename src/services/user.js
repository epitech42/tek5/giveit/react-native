import { AsyncStorage } from 'react-native';

export default class userService {
    static accessToken = 'access_token';

    static username = 'username';

    static userID = 'userID';

    static async SaveToken(token) {
      try {
        return await AsyncStorage.setItem(this.accessToken, token);
      } catch (e) {
        return null;
      }
    }

    static async LoadToken() {
      try {
        return await AsyncStorage.getItem(this.accessToken);
      } catch (e) {
        return null;
      }
    }

    static async SaveUsername(username) {
      try {
        return await AsyncStorage.setItem(this.username, username);
      } catch (e) {
        return null;
      }
    }

    static async LoadUsername() {
      try {
        return await AsyncStorage.getItem(this.username);
      } catch (e) {
        return null;
      }
    }

    static async SaveUserID(userID) {
      try {
        return await AsyncStorage.setItem(this.userID, userID);
      } catch (e) {
        return null;
      }
    }

    static async LoadUserID() {
      try {
        return await AsyncStorage.getItem(this.userID);
      } catch (e) {
        return null;
      }
    }

    static async Logout() {
      try {
        await AsyncStorage.removeItem(this.userID);
	    await AsyncStorage.removeItem(this.username);
	    await AsyncStorage.removeItem(this.accessToken);
	    return true;
      } catch (e) {
        return null;
      }
    }
}
