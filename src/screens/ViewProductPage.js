// import * as React from 'react';
import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Events from '../constants/events';

import Product from '../components/Product';

import userService from '../services/user';

const ViewProductPage = ({ route }) => {
  const navigation = useNavigation();
  const [product, setProduct] = useState({});
  const [refreshing, setRefreshing] = useState(false);
  const [isMine, setIsMine] = useState(false);

  const { id } = route.params;

  navigation.setOptions({
    title: route.params.title,
  });
  const socket = useSelector((state) => state.socketReducer.socket);

  useEffect(() => {
    socket.getProductInfos(id).then(() => {
	  socket.getServerAnswer(Events.productGetInfos).then(async (data) => {
	      const productRawData = data;
        // DATE FORMAT
        productRawData.updated = new Date(productRawData.updated).toLocaleString();
        productRawData.created = new Date(productRawData.created).toLocaleString();
        // USER ID OF MYSELF AND OBJECT
        const myUserID = await userService.LoadUserID();
        if (myUserID === data.createdBy) {
		  setIsMine(true);
        }
        socket.getUserFromID(data.createdBy).then(() => {
		  socket.getServerAnswer(Events.userGetUserFromIDEvent).then((user) => {
		      productRawData.createdBy = user.username;
		      setProduct(productRawData);
		  });
        });
        setRefreshing(false);
	  });
    });
  }, [refreshing, route.params.refreshing]);
  return (
    <Product product={product} isMine={isMine} />
  );
};

ViewProductPage.propTypes = {
  navigation: PropTypes.shape({
    setOptions: PropTypes.func,
  }).isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({
	    id: PropTypes.string,
	    title: PropTypes.string,
	    refreshing: PropTypes.bool,
    }),
  }).isRequired,
};

export default ViewProductPage;
