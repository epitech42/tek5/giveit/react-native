import {
  ActivityIndicator, StyleSheet, ScrollView, View, ToastAndroid,
} from 'react-native';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Col, Grid, Row } from 'react-native-easy-grid';
import {
  Button, Image, Input, Text,
} from 'react-native-elements';

import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { useActionSheet } from '@expo/react-native-action-sheet';
import * as FileSystem from 'expo-file-system';
import Events from '../constants/events';
import userService from '../services/user';
import ImageService from '../services/imageService';

const styles = StyleSheet.create({
  scrollView: {
    height: 800,
  },
  mainView: {
    height: 800,
  },
  grid: {
  },
  col: {
  },
  imageRow: {
    display: 'flex',
    height: 250,
    paddingBottom: 10,
  },
  addImageButton: {
    backgroundColor: '#3F4E50',
  },
  addImageButtonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
    borderRadius: 20,
  },
  addImageButtonTitle: {
    fontSize: 14,
  },
  submitButtonRow: {
    height: 140,
  },
  submitButtonContainer: {
    flex: 1,
    padding: 5,
  },
  submitButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'green',
    borderRadius: 10,
  },
  submitButtonIcon: {
    color: 'green',
  },
  submitButtonTitle: {
    color: 'green',
  },
  imageWrapper: {
    backgroundColor: 'lightgrey',
    flex: 1,
    height: '100%',
  },
  image: {
    height: '100%',
  },
  inputRow: {
  },
  inputView: {
    width: '100%',
  },
  activityIndicatorText: {
    paddingTop: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  validationText: {
    paddingLeft: 10,
    marginTop: -15,
    fontSize: 12,
    color: '#3F4E50',
  },
});

const ProfileEditPage = ({ route }) => {
  const navigation = useNavigation();

  const { showActionSheetWithOptions } = useActionSheet();

  const [username, setUsername] = useState();
  const [oldPassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [usernameValidation, setUsernameValidation] = useState();
  const [oldPasswordValidation, setOldPasswordValidation] = useState();
  const [passwordValidation, setPasswordValidation] = useState();
  const [confirmPasswordValidation, setConfirmPasswordValidation] = useState();

  const [loading, setLoading] = useState(false);

  const [picture, setPicture] = useState();
  const [imageUri, setImageUri] = useState();

  const socket = useSelector((state) => state.socketReducer.socket);

  useEffect(() => {
    socket.getMyProfile().then(() => {
      socket.getServerAnswer(Events.userGetUserFromUsernameEvent).then((response) => {
        setUsername(response.username);
        if (response.picture) {
          const pictureUrl = `https://cloudflare-ipfs.com/ipfs/${response.picture}`;
          fetch(pictureUrl).then((fetched) => {
            fetched.text().then((data) => {
              console.log('Picture size: ', data.length);
              setPicture(`data:image/png;base64,${data}`);
            });
          });
        }
      });
    });
  }, [route]);
  const onPressEditPicture = async () => {
    const imageService = new ImageService();
    const options = ['Take a photo', 'Pick from gallery', 'Cancel'];
    const cancelButtonIndex = 2;
    showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    }, async (buttonIndex) => {
      if (buttonIndex === 0) {
        const uri = await imageService.openCamera();
        const compressedImage = await imageService.compressImage(uri);
        setImageUri(compressedImage);
      } else if (buttonIndex === 1) {
        imageService.pickImage().then((uri) => {
          console.log(`uri ${uri}`);
          setImageUri(uri);
        }).catch((error) => {
          console.log(error);
          ToastAndroid.show(error.message, ToastAndroid.SHORT);
        });
      }
    });
  };
  const onPress = async () => {
    let usernameData; let passwordData; let
      pictureData = null;
    // EDIT USERNAME
    if (username.length > 0) {
      await socket.editUsername(username);
      usernameData = await socket.getServerAnswer(Events.userEditUsernameEvent);
    } else setUsernameValidation('The username is empty');
    // EDIT PASSWORD
    if (oldPassword.length > 0 && password.length > 0 && confirmPassword.length > 0) {
      if (password !== confirmPassword) {
        setPasswordValidation('The passwords are not matching');
        setConfirmPasswordValidation('The passwords are not matching');
        return;
      }
      await socket.editPassword(oldPassword, password);
      passwordData = await socket.getServerAnswer(Events.userEditPasswordEvent);
    } else if (oldPassword.length === 0) {
      setOldPasswordValidation('The password is empty');
    }
    // SET VALIDATION MESSAGES
    if (usernameData === 'USERNAME_ALREADY_TAKEN' && !(passwordData && pictureData)) {
      setUsernameValidation('The username you entered is already taken');
    }
    if (passwordData === 'OLD_PASSWORD_MISMATCH' && !(usernameData && pictureData)) {
      setOldPasswordValidation('You entered a wrong password');
    }
    // EDIT PICTURE
    if (imageUri) {
      setLoading(true);
      const base64File = await FileSystem.readAsStringAsync(imageUri, { encoding: 'base64' });
      await socket.uploadUserPicture(base64File);
      pictureData = await socket.getServerAnswer(Events.userUploadPictureEvent);
      setLoading(false);
      console.log(pictureData);
    }
    // SUCCESS
    if ((usernameData && usernameData.message === 'USER_EDITED')
          || (passwordData && passwordData.message === 'USER_EDITED')
          || (pictureData && pictureData.message === 'PICTURE_UPLOADED')) {
      if (usernameData && usernameData.message === 'USER_EDITED') {
        await userService.SaveUsername(username);
      }
      navigation.reset({
        index: 0,
        routes: [{ name: 'BottomTabNavigator', params: {} }],
      });
      ToastAndroid.show('Edited profile !', ToastAndroid.LONG);
    } else if (!(pictureData && usernameData && passwordData)) {
      ToastAndroid.show('No changes detected', ToastAndroid.LONG);
    }
  };
  return (
    <ScrollView style={styles.scrollView}>
      {loading
        ? (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator size="large" />
            <Text style={styles.activityIndicatorText}>Uploading profile picture</Text>
            <Text style={styles.activityIndicatorText}>Please wait...</Text>
          </View>
        )
        : (
          <View style={styles.mainView}>
            <Grid>
              <Col>
                <Row size={3}>
                  <View style={styles.imageWrapper}>
                    <Image
                      style={styles.image}
                      resizeMode="contain"
                      PlaceholderContent={<ActivityIndicator />}
                      source={{
                        uri: imageUri || (picture || 'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png'),
                      }}
                    />
                  </View>
                  <Button
                    onPress={onPressEditPicture}
                    titleStyle={styles.addImageButtonTitle}
                    containerStyle={styles.addImageButtonContainer}
                    buttonStyle={styles.addImageButton}
                    icon={{
                      name: 'camera-retro',
                      type: 'font-awesome',
                      size: 14,
                      color: 'white',
                    }}
                    title="Edit profile picture"
                  />
                </Row>
                <Row style={styles.inputRow} size={1}>
                  <View style={styles.inputView}>
                    <Input
                      defaultValue={username}
                      placeholder="Username"
                      onChangeText={(value) => setUsername(value)}
                      label="USERNAME"
                    />
                    {
                    usernameValidation
                      ? <Text style={styles.validationText}>{usernameValidation}</Text> : null
                }
                  </View>
                </Row>
                <Row style={styles.inputRow} size={1}>
                  <View style={styles.inputView}>
                    <Input
                      defaultValue={oldPassword}
                      placeholder="Leave empty for no changes"
                      onChangeText={(value) => setOldPassword(value)}
                      label="OLD PASSWORD"
                    />
                    {
                    oldPasswordValidation
                      ? <Text style={styles.validationText}>{oldPasswordValidation}</Text> : null
                }
                  </View>
                </Row>
                <Row style={styles.inputRow} size={1}>
                  <View style={styles.inputView}>
                    <Input
                      defaultValue={password}
                      placeholder="Leave empty for no changes"
                      onChangeText={(value) => setPassword(value)}
                      label="NEW PASSWORD"
                    />
                    {
                    passwordValidation
                      ? <Text style={styles.validationText}>{passwordValidation}</Text> : null
                }
                  </View>
                </Row>
                <Row style={styles.inputRow} size={1}>
                  <View style={styles.inputView}>
                    <Input
                      defaultValue={confirmPassword}
                      placeholder="Both password should match"
                      onChangeText={(value) => setConfirmPassword(value)}
                      label="CONFIRM NEW PASSWORD"
                    />
                    {
                    confirmPasswordValidation
                      ? <Text style={styles.validationText}>{confirmPasswordValidation}</Text>
                      : null
                }
                  </View>
                </Row>
                <Row style={styles.submitButtonRow} size={1}>
                  <Button
                    containerStyle={styles.submitButtonContainer}
                    buttonStyle={styles.submitButton}
                    titleStyle={styles.submitButtonTitle}
                    icon={{
                      iconStyle: styles.submitButtonIcon,
                      name: 'check',
                      type: 'font-awesome',
                      size: 15,
                      color: 'white',
                    }}
                    title="Submit"
                    onPress={onPress}
                  />
                </Row>
              </Col>
            </Grid>
          </View>
        )}
    </ScrollView>
  );
};


ProfileEditPage.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
    }),
  }).isRequired,
};


export default ProfileEditPage;
