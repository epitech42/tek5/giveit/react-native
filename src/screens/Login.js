import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, TouchableOpacity,
} from 'react-native';

import Form from '../components/Form';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  signupTextCont: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingVertical: 16,
    flexDirection: 'row',
  },
  signupText: {
    color: '#12799f',
    fontSize: 16,
  },
  signupButton: {
    color: '#12799f',
    fontSize: 16,
    fontWeight: '500',
  },
});

export default function Login({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>{'\n'}</Text>
      <Text>{'\n'}</Text>
      <Form type="Login" />
      <View style={styles.signupTextCont}>
        <Text style={styles.signupText}>Dont have an account yet? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Signup')}><Text style={styles.signupButton}>Signup</Text></TouchableOpacity>
      </View>
    </View>
  );
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};
