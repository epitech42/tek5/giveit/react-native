import React from 'react';

import ProductList from '../components/ProductList';

const AllProductsPage = () => (
  <ProductList type="All" />
);

export default AllProductsPage;
