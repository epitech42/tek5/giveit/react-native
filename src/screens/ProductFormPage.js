import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, ToastAndroid, ActivityIndicator,
} from 'react-native';
import {
  Input, Button, Image, Text,
} from 'react-native-elements';
import { Grid, Row, Col } from 'react-native-easy-grid';
import * as FileSystem from 'expo-file-system';
import { useActionSheet } from '@expo/react-native-action-sheet';
import { useSelector } from 'react-redux';
import Events from '../constants/events';
import ImageService from '../services/imageService';

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
  },
  grid: {
  },
  col: {
  },
  imageRow: {
    display: 'flex',
    height: 250,
    paddingBottom: 10,
  },
  addImageButton: {
    backgroundColor: '#3F4E50',
  },
  addImageButtonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
    borderRadius: 20,
  },
  addImageButtonTitle: {
    fontSize: 14,
  },
  submitButtonRow: {
    height: 140,
  },
  submitButtonContainer: {
    flex: 1,
    padding: 5,
  },
  submitButton: {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: 'green',
    borderRadius: 10,
  },
  submitButtonIcon: {
    color: 'green',
  },
  submitButtonTitle: {
    color: 'green',
  },
  imageWrapper: {
    backgroundColor: 'lightgrey',
    flex: 1,
    height: '100%',
  },
  image: {
    height: '100%',
  },
  inputRow: {
    height: 100,
  },
  activityIndicatorText: {
    paddingTop: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});

const ProductFormPage = ({ route }) => {
  const navigation = useNavigation();

  const socket = useSelector((state) => state.socketReducer.socket);

  const [title, setTitle] = useState();
  const [content, setContent] = useState();

  const [imageUri, setImageUri] = useState();
  const [decodedPicture, setDecodedPicture] = useState('');
  let pictureUrl = null;

  const [loading, setLoading] = useState(false);
  const { showActionSheetWithOptions } = useActionSheet();
  const [product, setProduct] = useState(null);

  useEffect(() => {
    if (route && route.params && route.params.product) {
      setProduct(route.params.product);
    }
    console.log('edit', product);
    if (product) {
      setTitle(product.title);
      setContent(product.content);
      if (product.picture) {
        pictureUrl = `https://cloudflare-ipfs.com/ipfs/${product.picture}`;
        fetch(pictureUrl).then((response) => {
          response.text().then((data) => {
            console.log('Picture size: ', data.length);
            setDecodedPicture(`data:image/png;base64,${data}`);
          });
        });
      }
    }
  }, [product, decodedPicture]);
  const onPress = async () => {
    console.log(title, content);
    if (title !== undefined && content !== undefined) {
      setLoading(true);
      let data = null;
      if (product) {
        await socket.editProduct(product._id, title, content);
        data = await socket.getServerAnswer(Events.productEditEvent);
      } else {
        await socket.addProduct(title, content);
        data = await socket.getServerAnswer(Events.productAddEvent);
      }
      console.log(data.message);
      if (data.message === 'PRODUCT_ADDED' || data.message === 'PRODUCT_EDITED') {
        if (imageUri) {
          const base64File = await FileSystem.readAsStringAsync(imageUri, { encoding: 'base64' });
          await socket.uploadProductPicture(data.productID, base64File);
          await socket.getServerAnswer(Events.productUploadPictureEvent);
        }
        if (data.message === 'PRODUCT_ADDED') {
          ToastAndroid.show(`Product ${title} added !`, ToastAndroid.SHORT);
        } else {
          ToastAndroid.show(`Product ${title} edited !`, ToastAndroid.SHORT);
        }
        setLoading(false);
        navigation.navigate('ViewProductPage', { id: data.productID, refreshing: true });
        // navigation.goBack();
      }
    }
  };
  const onPressEditCover = async () => {
    const imageService = new ImageService();
    const options = ['Take a photo', 'Pick from gallery', 'Cancel'];
    const cancelButtonIndex = 2;
    showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    }, async (buttonIndex) => {
      if (buttonIndex === 0) {
        const uri = await imageService.openCamera();
        const compressedImage = await imageService.compressImage(uri);
        setImageUri(compressedImage);
      } else if (buttonIndex === 1) {
        imageService.pickImage().then((uri) => {
          console.log(`uri ${uri}`);
	      setImageUri(uri);
        }).catch((error) => {
          console.log(error);
          ToastAndroid.show(error.message, ToastAndroid.SHORT);
        });
      }
    });
  };

  return (
    <View style={styles.mainView}>
      {loading
        ? (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator size="large" />
            {
              product ? <Text style={styles.activityIndicatorText}>Editing your product</Text>
                : <Text style={styles.activityIndicatorText}>Adding your product</Text>
            }
            <Text style={styles.activityIndicatorText}>Please wait...</Text>
          </View>
        )
        : (
          <View style={styles.mainView}>
            <Grid style={styles.grid}>
              <Col style={styles.col}>
                <Row style={styles.imageRow} size={6}>
                  <View style={styles.imageWrapper}>
                    <Image
                      style={styles.image}
                      resizeMode="contain"
                      PlaceholderContent={<ActivityIndicator />}
                      source={{
                        uri: imageUri || (decodedPicture || 'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png'),
                      }}
                    />
                  </View>
                  <Button
                    onPress={onPressEditCover}
                    titleStyle={styles.addImageButtonTitle}
                    containerStyle={styles.addImageButtonContainer}
                    buttonStyle={styles.addImageButton}
                    icon={{
                      name: 'camera-retro',
                      type: 'font-awesome',
                      size: 14,
                      color: 'white',
                    }}
                    title="Edit product cover"
                  />
                </Row>
                <Row style={styles.inputRow} size={2}>
                  <Input
                    defaultValue={product ? product.title : ''}
                    placeholder="Title"
                    onChangeText={(value) => setTitle(value)}
                    label="NAME"
                  />
                </Row>
                <Row style={styles.inputRow} size={4}>
                  <Input
                    defaultValue={product ? product.content : ''}
                    placeholder="Description"
                    multiline
                    maxLength={180}
                    onChangeText={(value) => setContent(value)}
                    label="DESCRIPTION"
                  />
                </Row>
                <Row style={styles.submitButtonRow} size={2}>
                  <Button
                    containerStyle={styles.submitButtonContainer}
                    buttonStyle={styles.submitButton}
                    titleStyle={styles.submitButtonTitle}
                    icon={{
                      iconStyle: styles.submitButtonIcon,
                      name: 'check',
                      type: 'font-awesome',
                      size: 15,
                      color: 'white',
                    }}
                    title="Submit"
                    onPress={async () => {
                      await onPress(imageUri);
                    }}
                  />
                </Row>
              </Col>
            </Grid>
          </View>
        )}
    </View>
  );
};

ProductFormPage.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func,
  }).isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({
      product: PropTypes.shape({
        created: PropTypes.string,
        updated: PropTypes.string,
        createdBy: PropTypes.string,
        title: PropTypes.string,
        content: PropTypes.string,
      }),
    }),
  }),
};


ProductFormPage.defaultProps = {
  route: {
    params: {
      product: {
        created: '',
        updated: '',
        createdBy: '',
        title: '',
        content: '',
      },
    },
  },
};


export default ProductFormPage;
