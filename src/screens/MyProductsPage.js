// import * as React from 'react';
import React from 'react';

import ProductList from '../components/ProductList';

const MyProductsPage = () => (
  <ProductList type="My" />
);

export default MyProductsPage;
