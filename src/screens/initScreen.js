import { useSelector } from 'react-redux';
import userService from '../services/user';

export default function ({ navigation }) {
  const socket = useSelector((store) => store.socketReducer.socket);

  userService.LoadToken().then((token) => {
    if (token != null) {
	    socket.auth().then(() => {
        navigation.reset({
		    index: 0,
		    routes: [{ name: 'BottomTabNavigator', params: {} }],
        });
	    });
    } else {
	    navigation.reset({
        index: 0,
        routes: [{ name: 'Login', params: {} }],
	    });
    }
  });
  return null;
}
