// import * as React from 'react';
import React, { useEffect, useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { Avatar, Divider, Button } from 'react-native-elements';

import Icon from '@expo/vector-icons/FontAwesome';

import { useSelector } from 'react-redux';

import Events from '../constants/events';

import userService from '../services/user';

const ProfilePage = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState();
  const [created, setCreated] = useState();
  const [updated, setUpdated] = useState();
  const [picture, setPicture] = useState();

  const socket = useSelector((state) => state.socketReducer.socket);
  const onPressMyProducts = () => {
    navigation.navigate('MyProductsPage', {});
  };

  const onPressLogout = () => {
    userService.Logout().then((value) => {
	  if (value === true) {
        navigation.reset({
		  index: 0,
		  routes: [{ name: 'Login' }],
        });
	  }
    });
  };

  const onPressEdit = () => {
    navigation.navigate('ProfileEditPage', { username });
  };
  useEffect(() => {
    socket.getMyProfile().then(() => {
      socket.getServerAnswer(Events.userGetUserFromUsernameEvent).then((data) => {
        setUsername(data.username);
        setCreated(new Date(data.created).toLocaleString());
        setUpdated(new Date(data.updated).toLocaleString());
        if (data.picture) {
          const pictureUrl = `https://cloudflare-ipfs.com/ipfs/${data.picture}`;
          fetch(pictureUrl).then((response) => {
            response.text().then((pictureData) => {
              console.log('Picture size: ', pictureData.length);
              setPicture(`data:image/png;base64,${pictureData}`);
            });
          });
        }
      });
    });
  }, []);

  return (
    <View>
      <View style={{ flexDirection: 'row' }}>
        {picture
          ? (
            <Avatar
              size="xlarge"
              title={username[0]}
              source={{ uri: picture }}
              containerStyle={{ flex: 1, marginLeft: 30, justifyContent: 'center' }}
            />
          )
          : (
            <Avatar
              size="xlarge"
              icon={{ name: 'user-circle', color: '#00CCFF', type: 'font-awesome' }}
              containerStyle={{ flex: 1, marginLeft: 30, justifyContent: 'center' }}
            />
          )}
        <View style={{ flexDirection: 'column', justifyContent: 'space-between' }}>
          <Button
            type="outline"
            raised
            containerStyle={{ }}
            icon={(
              <Icon
                name="sign-out"
                size={20}
                color="#FF0000"
              />
)}
            onPress={onPressLogout}
          />
          <Button
            type="outline"
            raised
            containerStyle={{ }}
            icon={(
              <Icon
                name="edit"
                size={20}
                color="#00ccff"
              />
              )}
            onPress={onPressEdit}
          />
        </View>
      </View>
      <Divider style={{ backgroundColor: 'grey' }} />
      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
        <Text style={{ fontSize: 12 }}>Username: </Text>
        <Text style={{ fontSize: 22 }}>{username}</Text>
      </View>
      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
        <Text style={{ fontSize: 12 }}>Created:</Text>
        <Text style={{ fontSize: 16 }}>{created}</Text>
      </View>
      <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
        <Text style={{ fontSize: 12 }}>Updated:</Text>
        <Text style={{ fontSize: 16 }}>{updated}</Text>
      </View>
      <Divider style={{ marginTop: 10, marginBottom: 25, backgroundColor: 'grey' }} />
      <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
        <Button
          title=" My Products"
          type="outline"
          raised
          icon={(
            <Icon
              name="th-list"
              size={20}
              color="#00ccff"
            />
)}
          onPress={onPressMyProducts}
        />
      </View>
    </View>
  );
};

ProfilePage.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    reset: PropTypes.func,
  }).isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({
    }),
  }).isRequired,
};


export default ProfilePage;
